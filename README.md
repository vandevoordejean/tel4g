# Installation

- `git clone git@bitbucket.org:vandevoordejean/tel4g.git`
- `cd path/to/project`
- `composer install`
- `chmod 777 -R var/{cache,logs,sessions}`
- `mysql -u <user> -p tel4g < data/db.sql`

# Documents

- [Cahier des charges technique](doc/cahier-des-charges.md)