$(function() {

    function montrerCacherBouton() {
        if ($('#btn-1,#btn-2,#btn-3').hasClass('btn-success')) {
            $('#btn-7').removeClass('invisible').addClass('visible');
        } else {
            $('#btn-7').removeClass('visible').addClass('invisible');
            $('#btn-4,#btn-5').removeClass('btn-success').addClass('btn-default');
        }
    }

    $('#btn-1').on('click',function () {
        $(this).toggleClass('btn-success');
            if ($('#btn-2, #btn-3').hasClass('btn-success')) {
               $('#btn-2, #btn-3').removeClass('btn-success').addClass('btn-default') 
            }
        montrerCacherBouton();
    });

    $('#btn-2').on('click',function () {
        $(this).toggleClass('btn-success');
        if ($('#btn-1, #btn-3').hasClass('btn-success')) {
            $('#btn-1, #btn-3').removeClass('btn-success').addClass('btn-default')
        }
        montrerCacherBouton();
    });

    $('#btn-3').on('click',function () {
        $(this).toggleClass('btn-success');
            if ($('#btn-1, #btn-2').hasClass('btn-success')) {
                $('#btn-1, #btn-2').removeClass('btn-success').addClass('btn-default');
           }
        montrerCacherBouton();
    });

    $('#btn-4').on('click', function() {
        if($('#btn-1,#btn-2,#btn-3,#btn-5').hasClass('btn-success')) {
            $(this).toggleClass('btn-success');
        } if ($('#btn-5').hasClass('btn-success')) {
                $('#btn-5').removeClass('btn-success').addClass('btn-default')
        }
    });       

    $('#btn-5').on('click', function() {
        if($('#btn-1,#btn-2,#btn-3').hasClass('btn-success')) {
            $(this).toggleClass('btn-success');
        }
        if ($('#btn-4').hasClass('btn-success')) {
            $('#btn-4').removeClass('btn-success').addClass('btn-default')
        }
    });

});