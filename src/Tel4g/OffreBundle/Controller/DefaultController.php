<?php

namespace Tel4g\OffreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use OffreBundle\Entity\Telephone;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/index", name="index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('Tel4gOffreBundle:Default:index.html.twig');
    }



    /**
     * @Route("/phones", name="phones")
     */
    public function showTelephone()
    {
        $telephone = $this
            ->getDoctrine()
            ->getRepository('Tel4gOffreBundle:Telephone')
            ->findAll()
        ;
        return $this->render('Tel4gOffreBundle:Default:boutique.html.twig',array (
                'telephone'=> $telephone
        ));
    }

    /**
     * @Route("/phone/{id}",name="/telephone1")
     */
    public function showChoixTelephone($id)
    {
        $telephone = $this
            ->getDoctrine()
            ->getRepository('Tel4gOffreBundle:Telephone')
            ->find($id)
        ;
        return $this->render('Tel4gOffreBundle:Default:choix.html.twig', array (
            'telephone'=> $telephone
        ));
    }


}