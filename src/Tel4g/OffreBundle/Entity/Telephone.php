<?php

namespace Tel4g\OffreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="telephone")
 */
class Telephone
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO");
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $poids;

        /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $autonomie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $os;

    /**
     * @ORM\Column(type="integer")
     */
    private $memoire;

        /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $extMem;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $norme4g;

            /**
     * @ORM\Column(type="string", length=255)
     */
    private $lien;


//*------------------------------setter et getter-----------------------------------------------------------*/
    /**
     * @return mixed
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * @param mixed $lien
     */
    public function setLien($lien)
    {
        $this->lien = $lien;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @param mixed $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }


    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }


    /**
     * @return mixed
     */
    public function getAutonomie()
    {
        return $this->autonomie;
    }

    /**
     * @param mixed $autonomie
     */
    public function setAutonomie($autonomie)
    {
        $this->autonomie = $autonomie;
    }

    /**
     * @return mixed
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * @param mixed $os
     */
    public function setOs($os)
    {
        $this->os = $os;
    }

    /**
     * @return mixed
     */
    public function getMemoire()
    {
        return $this->memoire;
    }

    /**
     * @param mixed $memoire
     */
    public function setMemoire($memoire)
    {
        $this->memoire = $memoire;
    }

    /**
     * @return mixed
     */
    public function getExtMem()
    {
        return $this->extMem;
    }

    /**
     * @param mixed $extMem
     */
    public function setExtMem($extMem)
    {
        $this->extMem = $extMem;
    }

    /**
     * @return mixed
     */
    public function getNorme4g()
    {
        return $this->norme4g;
    }

    /**
     * @param mixed $norme4g
     */
    public function setNorme4g($norme4g)
    {
        $this->norme4g = $norme4g;
    }

        /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

}