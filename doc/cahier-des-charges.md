# Cahier des charges technique

    
## Côté serveur
 1. **Php** : langage que l'on a appris à maîtrisé via les cours

## Côté client

 1. **Javascript** : pour les animations et  les interactions
 2. **Html** : on ne le présente plus 
 3. **jQuery**:  qui permet d'agir sur le code HTML, CSS et JavaScript 

## Base de données

 1. **Sql** :  pour la gestion de la base de données
 
## Design ##
 1. **Css** : pour le design du site 
 2. **Bootstrap** : Qui est un framework CSS, mais pas seulement, puisqu'il embarque également des composants HTML et JavaScript. Il comporte un système de grille simple et efficace pour mettre en ordre l'aspect visuel d'une page web. Il apporte du style pour les boutons, les formulaires, la navigation… Il permet ainsi de concevoir un site web rapidement et avec peu de lignes de code ajoutées.

## Framework

  Le framework utilisé sera **Symfony**  il s’agit d’une « boîte à outils » rassemblant des composants préfabriqués, rapides et faciles à utiliser. Cette boîte à outils facilite et accélère le développement de sites et d’applications Internet et Intranet, ce qui en fait un outil très recherché pour les développements de grosse ampleur.




